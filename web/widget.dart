import 'dart:html';

abstract class Widget {
	Widget parent;
	Element render();
}

class ListWidget {
	List<Widget> children;
	Element render() {
		var ret = new Element.div();
		children.forEach((widget){
			ret.append(widget.render());
		});
		return ret;
	}
}

class TextWidget extends Widget {
	String text;
	TextWidget(this.text);
	Element render(){
		return new Element.html(text);
	}
}

abstract class HtmlWidget extends Widget {
	Element render(){
		String html = renderHtml();
		//html.replaceAll(from, replace)
		/*html = html.replaceAllMapped(new RegExp(r'\<widget\skey\=\"\w+\"\/\>'), (match) {
  		return 'dupa'; //'"ASDF${match.group(0)}"';
		});*/
		var validator = new NodeValidatorBuilder.common();
		validator.allowElement('widget', attributes: ['key']);
		//validator.allowsElement(WidgetElement);
		var element = new Element.html(html, validator:validator);
		element.querySelectorAll('widget').forEach((e){
			print('x');
				print(e);
	print('x');
	});
		print(html);
		return element;
	}

	String renderHtml();
}

abstract class View {
	String title;
	Widget body;

}



class ExternalLink extends Widget {
  String href;
  Widget content;
  Element render() => (new AnchorElement(href: href)).append(content.render());
}

class AppLink extends Widget {
  String href;
  Widget content;
  Element render() {
    var a = new AnchorElement(href: href);
    a.onClick.listen((MouseEvent event){
      window.history.pushState({}, 'Kaszana', href);
    });
    a.append(content.render());
    return a;
  }
}
